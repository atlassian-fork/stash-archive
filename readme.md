* * *

**This add-on is deprecated.** Bitbucket Server 5.1 includes archive functionality in the base product, and obsoletes
this add-on. If this add-on is installed and Bitbucket Server is upgraded to 5.1.0 or newer, the add-on will be
_automatically_ uninstalled by the upgrade. Bitbucket Server prevents obsolete plugins from being reinstalled, so
after an upgrade to Bitbucket Server 5.1 it is no longer possible to use this add-on.

For details about the embedded archive support, see [BSERV-2732](https://jira.atlassian.com/browse/BSERV-2732).
Bugs and feature requests for the _embedded_ archive functionality should be created as `BSERV` issues, at
[jira.atlassian.com](https://jira.atlassian.com/projects/BSERV/issues).

* * *

# Bitbucket Server Archive Plugin

Adds a sidebar button to repository views which allows downloading a zip archive of the source, and a REST API with
some additional features. It also adds a link to the actions dropdown for branch list entries allowing the source for
any branch in the list to be downloaded.

### Download Button

The Download button in the sidebar after Fork on all repository views. On pages where a branch selector is present,
clicking the button will download a zip of the source at whatever branch or tag is active in the selector. Changing
the selected branch or tag will automatically update the download link.

The Download button is also shown When viewing a specific commit in the repository, and clicking the button will
download a zip of the source at that commit.

### REST API

The basic form for an archive REST URL is:

```https://<bitbucket-base-url>/rest/archive/1.0/projects/<projectKey>/repos/<repoSlug>/archive```

This will download a file named `<repoSlug>-<defaultBranchName>@<latestCommit>.zip` that contains the source at the
tip of the repository's default branch.

You can change the output using the following query parameters:

* `at=<branch|ref|sha|tag|tree>` will specify the ref to download, which may be specified using any Git "tree-ish"
* `format=[zip|tar|tar.gz]` will change the output format (default is zip if not specified)
* `filename=<name>` will change the name of the downloaded file
  * If a `filename` is not specified, the format used to create a default depends on the `at` value
  * If `at` is not specified, or if it identifies a branch or a tag, the format is `<repoSlug>-<refName>@<latestCommit>.<format>`
  * If `at` is any other value, such as a commit or tree SHA, the format is `<repoSlug>-<at>.<format>`
* `path` will filter the archive to only include files matching the path; may be supplied more than once to limit
  the archive to a set of paths
* `prefix=<prefix>` will be prepended to each filename in the archive
  * If the provided `prefix` does not include a trailing `/`, one is added automatically to ensure the prefix is
    always treated as directory

For example, the URL:

```https://<bitbucket-base-url>/rest/archive/1.0/projects/TEST/repos/my-cool-repo?at=release-1.3.0&filename=cool-1.3.0.tar.gz&format=tar.gz```

Will yield `cool-1.3.0.tar.gz`, containing the repository contents at the `release-1.3.0` tag.

## Configuration

By default, archive streaming is aborted on the server if:

* It is idle for more than _5 minutes_, where "idle" means the remote client is not consuming bytes
* Total execution takes more than _30 minutes_ to complete

By comparison, a hosting operation (such as `git fetch`) is allowed to be idle for 30 minutes, with a maximum
execution time of _one day_, before it is aborted. Default archive timeouts are intentionally more aggressive
than those applied to other hosting operations.

For instances with extremely large repositories, it may be necessary to adjust these timeouts. Each timeout can be
configured separately, using the following properties:

```
# Controls the _total execution timeout_ for archive processes. The default is _30 minutes_.
# If the configured value is less than _5 minutes_ it will be ignored and 5 minutes will be
# used as the timeout.
# This value is in **seconds**.
plugin.bitbucket-archive.timeout.execution=1800

# Controls the _idle timeout_ for archive processes, which is used to terminate processes for
# which the client is still connected but not consuming bytes. The default is _5 minutes_. If
# the configured value is less than _1 minute_, it will be ignored and 1 minute will be used
# as the timeout.
# This value is in **seconds**.
plugin.bitbucket-archive.timeout.idle=300
```

## Throttling

Downloading an archive can be a heavyweight operation, especially for large repositories. As such, it is throttled
by the server using the same "scm-hosting" ticket as a `git clone`, `git fetch`, `git pull` or `git push`. Since
all of these operations use the same ticket type, they can block each other. _This is intentional_, and is intended
to help preserve server resources.

package com.atlassian.bitbucket.internal.archive.rest;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

/**
 * Adds support for {@link ArchiveResponse} entities on {@code Response}s.
 * <p>
 * This provider is registered with JAX-RS via an entry in {@code services/javax.ws.rs.ext.MessageBodyWriter}
 * under {@code META-INF}.
 *
 * @since 2.1
 */
@Produces({"application/octet-stream", "*/*"})
public class ArchiveResponseProvider implements MessageBodyWriter<ArchiveResponse> {

    @Override
    public long getSize(ArchiveResponse archiveResponse, Class<?> type, Type genericType,
                        Annotation[] annotations, MediaType mediaType) {
        return -1;
    }

    @Override
    public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return ArchiveResponse.class.isAssignableFrom(type);
    }

    @Override
    public void writeTo(ArchiveResponse response, Class<?> type, Type genericType, Annotation[] annotations,
                        MediaType mediaType, MultivaluedMap<String, Object> httpHeaders, OutputStream entityStream)
            throws IOException, WebApplicationException {
        response.write(httpHeaders, entityStream);
    }
}
package com.atlassian.bitbucket.internal.archive.rest;

import javax.annotation.Nonnull;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MultivaluedMap;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Allows streaming a response directly, similar to {@code StreamingOutput}. Unlike {@code StreamingOutput}, though,
 * {@code ArchiveResponse} allows HTTP headers to be set prior to streaming. Setting headers in {@link #write}
 * instead of on the {@code Response} wrapping this {@code ArchiveResponse}, allows setting headers only if the
 * operations leading up to streaming bytes actually succeed.
 *
 * @since 2.1
 */
public interface ArchiveResponse {

    /**
     * Writes the entity payload to the response, optionally setting additional HTTP headers first.
     * <p>
     * Additional headers <i>must</i> be set before the first byte is written to the provided {@code OutputStream}.
     * When the first byte is written pending headers are flushed, and any subsequent headers <i>will be ignored</i>.
     *
     * @param httpHeaders  a <i>mutable</i> map of HTTP headers, allowing new headers to be set (HTTP headers can
     *                     only be set prior to writing any bytes to the provided {@code OutputStream}.)
     * @param outputStream the stream to write the entity payload to
     * @throws IOException             if the entity cannot be written to the stream
     * @throws WebApplicationException if an unexpected error occurs and the overall {@code Response} should be
     *                                 changed (The response can only be changed if this exception is thrown prior
     *                                 to writing any bytes to the provided {@code OutputStream}.)
     */
    void write(@Nonnull MultivaluedMap<String, Object> httpHeaders, @Nonnull OutputStream outputStream)
            throws IOException, WebApplicationException;
}
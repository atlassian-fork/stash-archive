package com.atlassian.bitbucket.archive;

import com.atlassian.bitbucket.NoSuchEntityException;
import com.atlassian.bitbucket.io.TypeAwareOutputSupplier;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.throttle.ResourceBusyException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.OutputStream;

/**
 * Service for handling {@code git archive} requests.
 */
public interface ArchiveService {

    /**
     * Stream the content of a repository at a particular ref in an archive file format.
     *
     * @param repository the repository containing the specified ref
     * @param format the {@link ArchiveFormat} used to compress the content
     * @param ref the ref specifying the content to stream
     * @param outputStream the output stream to stream the archive file to.
     * @throws NoSuchEntityException if the specified ref doesn't exist
     * @throws ResourceBusyException if the server is under too much load to process an archive command at the moment
     * @deprecated in 2.1 for removal in 3.0. Use {@link #stream(ArchiveRequest, TypeAwareOutputSupplier)} instead.
     */
    @Deprecated
    void stream(@Nonnull Repository repository, @Nullable ArchiveFormat format, @Nonnull String ref,
                @Nonnull OutputStream outputStream) throws NoSuchEntityException, ResourceBusyException;

    /**
     * Streams an archive of the specified repository at the specified commit, optionally filtering by path. The
     * content type for the requested archive format will be provided to the supplier when retrieving the output
     * stream to which the archive will be written.
     *
     * @param request        describes the commit to archive, the format to archive it in, and the repository to
     *                       archive it from
     * @param outputSupplier a supplier which, when given the archive content type, will provide an output stream
     * @since 2.1
     */
    void stream(@Nonnull ArchiveRequest request, @Nonnull TypeAwareOutputSupplier outputSupplier);
}

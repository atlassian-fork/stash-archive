package com.atlassian.bitbucket.archive;

import com.atlassian.bitbucket.commit.Commit;
import com.atlassian.bitbucket.repository.Branch;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.Tag;
import com.atlassian.bitbucket.util.BuilderSupport;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Optional;
import java.util.Set;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;

/**
 * Describes a request to download an archive of a given repository at a specified commit, optionally filtered to
 * only include certain paths.
 *
 * @since 2.1
 */
public class ArchiveRequest {

    private final String commitId;
    private final ArchiveFormat format;
    private final Set<String> paths;
    private final String prefix;
    private final Repository repository;

    private ArchiveRequest(Builder builder) {
        commitId = builder.commitId;
        format = builder.format;
        paths = builder.paths.build();
        prefix = StringUtils.appendIfMissing(builder.prefix, "/"); //Has no effect if prefix is null
        repository = builder.repository;
    }

    /**
     * @return the commit to archive, which may be identified by {@link Branch#getId branch ID}, {@link Tag#getId
     *         tag ID} or {@link Commit#getId() commit ID}
     */
    @Nonnull
    public String getCommitId() {
        return commitId;
    }

    /**
     * Retrieves the format to create the archive in. The underlying SCM may support additional types, but every
     * SCM which supports archives is <i>required</i> to support every {@link ArchiveFormat standard format}.
     *
     * @return the format to create the archive in
     */
    @Nonnull
    public ArchiveFormat getFormat() {
        return format;
    }

    /**
     * Retrieves a set of paths to filter the archive by. The exact approach to filtering by path is
     * <i>SCM-specific</i>, and SCM implementations are <i>not required</i> to support filtering if
     * the underlying SCM doesn't.
     *
     * @return a set of paths to filter the archive by, which may be empty but never {@code null}
     */
    @Nonnull
    public Set<String> getPaths() {
        return paths;
    }

    /**
     * Retrieves a prefix to apply to each entry in the archive. If a prefix is supplied, it will be prepended
     * to all archived files and folders.
     * <p>
     * Since the most common use case for a prefix is to add a top-level folder to the archive, if the prefix
     * supplied does not end with a {@code /}, one is added automatically. Without a trailing {@code /} the
     * prefix is <i>not</i> considered a top-level directory; instead it modifies the file names. For example,
     * consider a repository containing the following files and folders:
     * <code><pre>
     * src/
     *   main/
     *     java/
     *       Example.java
     * pom.xml
     * README.md
     * </pre></code>
     * Without normalization, a prefix of "example" would result in an archive containing:
     * <code><pre>
     * examplesrc/
     *   main/
     *     java/
     *       Example.java
     * examplepom.xml
     * exampleREADME.md
     * </pre></code>
     * That's unlikely to be what the caller intended. Instead, a trailing {@code /} is appended, resulting in
     * "example/" for the prefix, and the archive contains the following instead:
     * <code><pre>
     * example/
     *   src/
     *     main/
     *       java/
     *         Example.java
     *   pom.xml
     *   README.md
     * </pre></code>
     *
     * @return an optional prefix to prepend to the archive's contents, which may be {@code empty}
     *         but never {@code null}
     */
    @Nonnull
    public Optional<String> getPrefix() {
        return ofNullable(prefix);
    }

    /**
     * @return the repository to archive
     */
    @Nonnull
    public Repository getRepository() {
        return repository;
    }

    public static class Builder extends BuilderSupport {

        private final String commitId;
        private final ImmutableSet.Builder<String> paths;
        private final Repository repository;

        private ArchiveFormat format;
        private String prefix;

        public Builder(@Nonnull ArchiveRequest request) {
            this(requireNonNull(request, "request").getRepository(), request.getCommitId());

            format(request.getFormat())
                    .paths(request.getPaths())
                    .prefix(request.getPrefix().orElse(null));
        }

        public Builder(@Nonnull Repository repository, @Nonnull String commitId) {
            this.repository = requireNonNull(repository, "repository");
            this.commitId = requireNonNull(commitId, "commitId");

            format = ArchiveFormat.ZIP;
            paths = ImmutableSet.builder();
        }

        @Nonnull
        public ArchiveRequest build() {
            return new ArchiveRequest(this);
        }

        @Nonnull
        public Builder format(@Nonnull ArchiveFormat value) {
            format = requireNonNull(value, "format");

            return this;
        }

        @Nonnull
        public Builder path(@Nullable String value) {
            addIf(NOT_BLANK, paths, value);

            return this;
        }

        @Nonnull
        public Builder paths(@Nullable Iterable<String> values) {
            addIf(NOT_BLANK, paths, values);

            return this;
        }

        @Nonnull
        public Builder paths(@Nullable String value, @Nullable String... values) {
            addIf(NOT_BLANK, paths, value, values);

            return this;
        }

        @Nonnull
        public Builder prefix(@Nullable String value) {
            prefix = StringUtils.trimToNull(value);

            return this;
        }
    }
}
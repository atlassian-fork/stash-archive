package com.atlassian.bitbucket.internal.archive;

import com.atlassian.bitbucket.archive.ArchiveRequest;
import com.atlassian.bitbucket.i18n.I18nService;
import com.atlassian.bitbucket.i18n.SimpleI18nService;
import com.atlassian.bitbucket.io.TypeAwareOutputSupplier;
import com.atlassian.bitbucket.repository.InvalidRefNameException;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.scm.git.command.GitCommandBuilderFactory;
import com.atlassian.bitbucket.server.ApplicationPropertiesService;
import com.atlassian.bitbucket.throttle.ResourceBusyException;
import com.atlassian.bitbucket.throttle.ThrottleService;
import com.atlassian.bitbucket.validation.ArgumentValidationException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.stream.Stream;

import static com.atlassian.bitbucket.i18n.SimpleI18nService.Mode.RETURN_KEYS_WITH_ARGUMENTS;
import static com.atlassian.bitbucket.util.MoreCollectors.toImmutableList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("unused")
public class GitArchiveServiceTest {

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @InjectMocks
    private GitArchiveService archiveService;
    @Mock
    private GitCommandBuilderFactory builderFactory;
    @Spy
    private I18nService i18nService = new SimpleI18nService(RETURN_KEYS_WITH_ARGUMENTS);
    @Mock
    private ApplicationPropertiesService propertiesService;
    @Mock(name = "repository")
    private Repository repository;
    @Mock
    private ThrottleService throttleService;

    @Test
    public void testStreamThrowsOnFailureToAcquireTicket() {
        expectedException.expect(ResourceBusyException.class);

        doThrow(ResourceBusyException.class).when(throttleService).acquireTicket(eq("scm-hosting"));

        TypeAwareOutputSupplier outputSupplier = mock(TypeAwareOutputSupplier.class);
        try {
            archiveService.stream(new ArchiveRequest.Builder(repository, "master").build(), outputSupplier);
        } finally {
            verifyZeroInteractions(outputSupplier);
        }
    }

    @Test
    public void testStreamWithInvalidRef() {
        List<String> invalidRefIds = Stream.of("", " ", "\u0009", "\u000b")
                .map(prefix -> prefix + "--output=invalid-ref.tar.gz")
                .collect(toImmutableList());
        TypeAwareOutputSupplier outputSupplier = mock(TypeAwareOutputSupplier.class);

        for (String invalidRefId : invalidRefIds) {
            try {
                archiveService.stream(new ArchiveRequest.Builder(repository, invalidRefId).build(), outputSupplier);

                fail("'" + invalidRefId + "' should have triggered an InvalidRefNameException");
            } catch (InvalidRefNameException e) {
                assertEquals("bitbucket.archive.invalidref", e.getMessageKey());
                assertEquals("bitbucket.archive.invalidref(" + invalidRefId + ")", e.getMessage());
            } finally {
                verifyZeroInteractions(outputSupplier);
            }
        }
    }
}
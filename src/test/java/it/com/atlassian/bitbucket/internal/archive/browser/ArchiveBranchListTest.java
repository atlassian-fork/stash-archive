package it.com.atlassian.bitbucket.internal.archive.browser;

import com.atlassian.bitbucket.test.DefaultFuncTestData;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.webdriver.bitbucket.element.BranchList;
import com.atlassian.webdriver.bitbucket.page.BranchListPage;
import com.google.common.base.Throwables;
import org.junit.Test;
import org.openqa.selenium.By;

import java.lang.reflect.Field;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ArchiveBranchListTest extends QaRepositoryTest {

    private static final Field FIELD_ENTRY_ELEMENT;
    private static final Field FIELD_LIST_ELEMENT_FINDER;

    static {
        try {
            FIELD_ENTRY_ELEMENT = BranchList.BranchListEntry.class.getDeclaredField("element");
            FIELD_ENTRY_ELEMENT.setAccessible(true);

            FIELD_LIST_ELEMENT_FINDER = BranchList.class.getDeclaredField("elementFinder");
            FIELD_LIST_ELEMENT_FINDER.setAccessible(true);
        } catch (Exception e) {
            throw Throwables.propagate(e);
        }
    }

    /**
     * Downloading links in Selenium is complicated. Since we don't care too much about the content of the archive,
     * this test just verifies that the link is as expected. If the link is missing, or is to the wrong branch, we
     * should catch that here.
     */
    @Test
    public void testDownloadAction() {
        String archiveUrl = DefaultFuncTestData.getRepositoryRestURL("archive", "latest", getProjectKey(), getRepoSlug()) + "/archive";

        BranchListPage page = loginAsAdmin(BranchListPage.class, getProjectKey(), getRepoSlug());
        BranchList branchList = page.getBranchList();

        // The default row should not have an "at" query parameter
        BranchList.BranchListEntry defaultEntry = branchList.getDefaultBranchRow();
        assertNotNull(defaultEntry);
        assertEquals(archiveUrl + "?format=zip", getDownloadLinkHref(branchList, defaultEntry));

        // Other rows should have an "at" query parameter to download the correct branch
        BranchList.BranchListEntry branchListEntry = branchList.getRows()
                .stream()
                .filter(entry -> !entry.getName().equals(defaultEntry.getName()))
                .findFirst()
                .orElseThrow(() -> new AssertionError("Unable to find a non-default branch"));

        assertEquals(archiveUrl + "?at=" + getId(branchListEntry).replace("/", "%2F") + "&format=zip",
                getDownloadLinkHref(branchList, branchListEntry));
    }

    //BranchList and BranchListEntry don't do a very good job of facilitating testing of extensions to the
    //actions dropdown, despite the fact that it's a documented plugin point. Rather than building our own
    //PageObjects, all of this ugly Reflection allows us to "extend" the entry with the ability to get the
    //"Download" action
    private static String getDownloadLinkHref(BranchList list, BranchList.BranchListEntry entry) {
        PageElement element = getField(entry, FIELD_ENTRY_ELEMENT);
        PageElement actionsTrigger = element.find(By.className("branch-list-action-trigger"));
        actionsTrigger.click();

        PageElementFinder elementFinder = getField(list, FIELD_LIST_ELEMENT_FINDER);
        PageElement menu = elementFinder.find(By.id("branch-list-actions-menu-refs/heads/" + entry.getName()));
        Poller.waitUntilTrue(menu.timed().isVisible());

        PageElement downloadAction = menu.find(By.className("download-repo"));
        // click the menu a second time to close it.
        actionsTrigger.click();
        return downloadAction.isPresent() ? downloadAction.getAttribute("href") : null;
    }

    @SuppressWarnings("unchecked")
    private static <T> T getField(Object instance, Field field) {
        try {
            return (T) field.get(instance);
        } catch (IllegalAccessException e) {
            throw Throwables.propagate(e);
        }
    }

    private static String getId(BranchList.BranchListEntry entry) {
        PageElement element = getField(entry, FIELD_ENTRY_ELEMENT);

        return element.getAttribute("data-id");
    }
}

package it.com.atlassian.bitbucket.internal.archive.browser;

import com.atlassian.bitbucket.test.RepositoryTestHelper;
import org.junit.BeforeClass;
import org.springframework.core.io.ClassPathResource;

public abstract class QaRepositoryTest extends AbstractBrowserTest {

    private static final String PROJECT_KEY_PREFIX = "QA_TEST-";
    private static final String REPO_SLUG_SUFFIX = "qa-resources-";

    private static volatile RepositoryCoordinates coordinates;

    @BeforeClass
    public static void initRepository() throws Exception {
        coordinates = new RepositoryCoordinates();

        getClassTestContext()
               .project(coordinates.projectKey, "QA Test Project")
               .repository(coordinates.projectKey, coordinates.repoSlug, new ClassPathResource(RepositoryTestHelper.QA_RESOURCES_REPOSITORY));
    }

    static String getProjectKey() {
        return coordinates.projectKey;
    }

    static String getRepoSlug() {
        return coordinates.repoSlug;
    }

    private static class RepositoryCoordinates {

        private final String projectKey;
        private final String repoSlug;

        private RepositoryCoordinates() {
            String suffix = Long.toString(System.currentTimeMillis() % 10000);
            this.projectKey = PROJECT_KEY_PREFIX + suffix;
            this.repoSlug = REPO_SLUG_SUFFIX + suffix;
        }
    }

}

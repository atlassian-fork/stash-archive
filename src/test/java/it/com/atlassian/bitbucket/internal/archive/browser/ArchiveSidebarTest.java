package it.com.atlassian.bitbucket.internal.archive.browser;

import com.atlassian.webdriver.bitbucket.element.AUISidebar;
import com.atlassian.webdriver.bitbucket.element.LinkElement;
import com.atlassian.webdriver.bitbucket.page.*;
import org.junit.Test;

import static com.atlassian.bitbucket.test.DefaultFuncTestData.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Verifies that the "Download" button appears in the sidebar on the browse, commits and branches pages, and
 * that updating the branch selector updates the download links on each, for both branches and tags.
 */
public class ArchiveSidebarTest extends AbstractBrowserTest {

    private static final String ARCHIVE_URL = getRepositoryRestURL("archive",
            "latest", getProject1(), getProject1Repository1()) + "/archive";

    /**
     * Verifies that the "Download" button appears in the sidebar on the commit page, and that it downloads the
     * commit being viewed. There's no branch selector on this page, so the link can't be updated.
     */
    @Test
    public void testDownloadUrlOnCommitPage() {
        CommitUnifiedDiffPage commitPage = loginAsAdmin(CommitUnifiedDiffPage.class,
                getProject1(), getProject1Repository1(), "8a71bdc3c67e49729db5f17f0477cee857fd7ae7");

        LinkElement downloadButton = getDownloadButton(commitPage);
        assertEquals(ARCHIVE_URL + "?at=8a71bdc3c67e49729db5f17f0477cee857fd7ae7&format=zip", downloadButton.getUrl());
    }

    @Test
    public void testDownloadUrlsOnBranchesPage() {
        runUrlsTest(BranchListPage.class);
    }

    @Test
    public void testDownloadUrlsOnBrowsePage() {
        runUrlsTest(FileBrowserPage.class);
    }

    @Test
    public void testDownloadUrlsOnCommitsPage() {
        runUrlsTest(CommitListPage.class);
    }

    private static void closeSidebar(BaseSidebarPage page) {
        AUISidebar sidebar = page.getSidebar();
        if (!sidebar.isCollapsed()) {
            sidebar.toggle();
        }
    }

    private static LinkElement getDownloadButton(BaseSidebarPage page) {
        LinkElement downloadButton = openSidebar(page).getActionsLinks().stream()
                .filter(link -> "Download".equals(link.getKey()))
                .findFirst()
                .orElse(null);

        assertNotNull("The Download link is not in the sidebar", downloadButton);

        closeSidebar(page);

        return downloadButton;
    }

    private static AUISidebar openSidebar(BaseSidebarPage page) {
        AUISidebar sidebar = page.getSidebar();
        if (sidebar.isCollapsed()) {
            sidebar.toggle();
        }

        return sidebar;
    }

    private <T extends BranchLayoutPage> void runUrlsTest(Class<T> pageClass) {
        T page = loginAsAdmin(pageClass, getProject1(), getProject1Repository1());

        //For the default branch, which should be selected when the page loads, no ref should be included
        //in the archive URL
        LinkElement downloadButton = getDownloadButton(page);
        assertEquals(ARCHIVE_URL + "?format=zip", downloadButton.getUrl());

        //Select a branch and verify the link updates
        page.getBranchSelector().open().selectItemByName("basic_branching");
        assertEquals(ARCHIVE_URL + "?at=refs%2Fheads%2Fbasic_branching&format=zip", downloadButton.getUrl());

        //Set a tag and verify the link updates
        page.getBranchSelector().open().selectTags().selectItemByName("retagged_signed_tag");
        assertEquals(ARCHIVE_URL + "?at=refs%2Ftags%2Fretagged_signed_tag&format=zip", downloadButton.getUrl());
    }
}
